import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });
  const profile = document.querySelector(".image");
  profile.addEventListener("click", () => {
    profile.style.transform = "scale(2)"
  })
});
